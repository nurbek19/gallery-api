const express = require('express');
const multer = require('multer');
const router = express.Router();
const path = require('path');
const nanoid = require('nanoid');
const Photo = require('../models/Photo');
const config = require('../config');

const auth = require('../middleware/auth');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = () => {
    router.get('/', (req, res) => {
        const userId = req.query.user;

        if(userId) {
            Photo.find({user: userId}).populate('user')
                .then(results => res.send(results))
                .catch(() => res.sendStatus(500));
        } else {
            Photo.find().populate('user').populate('user')
                .then(results => res.send(results))
                .catch(() => res.sendStatus(500));
        }
    });

    router.post('/', [auth, upload.single('image')], (req, res) => {
        const photo = req.body;
        photo.user = req.user._id;

        if (req.file) {
            photo.image = req.file.filename;
        } else {
            photo.image = null;
        }

        const p = new Photo(photo);

        p.save()
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error))
    });

    router.delete('/:id', auth, (req, res) => {
        Photo.deleteOne({_id: req.params.id})
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error))
    });

    return router;
};

module.exports = createRouter;