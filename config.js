const path = require('path');

const rootPath = __dirname;

module.exports = {
    rootPath,
    uploadPath: path.join(rootPath, 'public/uploads'),
    db: {
        url: 'mongodb://localhost:27017',
        name: 'gallery'
    },
    jwt: {
        secret: 'some kinda very secret string',
        expires: '1h'
    },
    facebook: {
        appId: '943016832539466',
        appSecret: '7c9d16534b637d1eb2e515925b0bbdb9'
    }
};