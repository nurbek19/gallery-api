const mongoose = require('mongoose');
const config = require('./config');

const Photo = require('./models/Photo');
const User = require('./models/User');

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', async () => {
    try {
        await db.dropCollection('photos');
        await db.dropCollection('users');
    } catch (e) {
        console.log('Collections were not present, skipping drop...');
    }

    const [Nurbek, Joldosh] = await User.create({
        username: 'Nurbek',
        password: '123'
    }, {
        username: 'Joldosh',
        password: '123'
    });

    await Photo.create({
        title: 'Everest',
        image: 'everest.jpg',
        user: Nurbek._id
    }, {
        title: 'Issyk-Kul lake',
        image: 'issyk-kul.jpg',
        user: Nurbek._id
    }, {
        title: 'Kyrgyzstan',
        image: 'nature.jpg',
        user: Joldosh._id
    }, {
        title: 'Ocean',
        image: 'ocean.jpeg',
        user: Joldosh._id
    });

    db.close();
});